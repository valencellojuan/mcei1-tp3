```
while true                                                  //Inicio la condición de loop infinito. Se puede salir de el usando ctrl+v en la terminal.
do                                                          //Abro el loop

tar -zcf "imgs_20"$(date +"%y%m%d_%H%M%S")".tar" ./dataSet  //creo el backup con el formato de ejemplo
								                            // especificado en las consignas del tp. 
sleep 15m	                                                // espera 15 minutos para hacer el backup

done                                                        //Cierro el loop
```