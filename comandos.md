 # Comandos utilizados para la resolución del TP3

El directorio de trabajo inicial es tp3-data (El directorio clonado para la práctica)

 ## Comandos ejecutados:
```
cd ./fake							//Cambio el directorio de trabajo a ./fake
for file in mid_* ;						//Inicio un loop para cada archivo dentro de /fake/ que inicie con mid_
do newname=img_"$(echo "$file" | cut -c5-)" ;			//Almaceno en el string "newname" el nombre que empieza con img_
echo $file "fue convertido a" $newname >> renameFake.dat ;	//Inicializo, y voy actualizando, el log con nombres originales y cambiados.
mv "$file" "$newname" ;						//Cambio el nombre inicial del archivo actual al nombre modificado
done								//Cierro el loop
ls								//verifico que los nombres hayan cambiado correctamente.

cd ../real							//Cambio el directorio de trabajo a ./real/
for file in real_* ;						//Inicio un loop para cada archivo dentro de /real/ que inicie con real_ 
do newname=img_"$(echo "$file" | cut -c6-)" ;			//Almaceno en el string "newname" el nombre que empieza con img_
echo $file "fue convertido a" $newname >> renameReal.dat ;	//Inicializo, y voy actualizando, el log con nombres originales y cambiados
mv "$file" "$newname" ;						//Cambio el nombre inicial del archivo actual al nombre modificado
done								//Cierro el loop
ls								//Verifico que los nombres hayan cambiado correctamente

cd ..								//Vuelvo al directorio de trabajo /tp3-data/
mkdir ./dataSet							//Creo el directorio ./dataSet/

cd ./fake							//Cambio directorio de trabajo a ./fake
for file in *							//Inicio un loop para todo archivo dentro de /fake/
do mv "$file" ../dataSet ;					//Muevo cada archivo dentro de /fake/ a ../dataSet
done								//Cierro el loop
ls								//Verifico que los archivos hayan sido movidos

cd ../real							//Cambio de directorio de trabajo a ../real
for file in *							//Inicio un loop para todo archivo dentro de /real/
do mv "$file" ../dataSet ;					//Muevo cada archivo dentro de /real/ a ../dataSet
done								//Cierro el loop
ls								//Verifico que los archivos hayan sido movidos

cd ..								//Vuelvo al directorio /tp3-data/
ls ./dataSet							//Verifico que ./dataSet tenga los archivos movidos anteriormente

man tar								//Abro el manual para entender el uso del comando 'tar'
man date							//Abro el manual para entender el uso del comando 'date'
tar -zcf "imgs_20"$(date +"%y%m%d_%H%M%S")".tar" ./dataSet	//Creo un .tar siguiendo el patron del ejemplo de las consignas del tp3

```