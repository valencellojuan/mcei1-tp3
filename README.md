# Métodos computacionales para ingeniería 1 - UNComa (2020)
# Práctica N° 3

Este repositorio contiene mi Trabajo Práctico N°3 para la materia. Las consignas de este trabajo se encuentran disponibles en -https://docs.google.com/document/d/1iklQ58khUa_JTN5SPMCEUT3DogHSeUo_xNyRWr5Oy7o/edit

El archivo "backupscript.sh" corresponde al script para realizar backups de los archivos en ./dataSet cada 15 minutos. Este es un loop infinito, y el proceso se ejecutará permanentemente hasta salir forzosamente del loop usando ctrl+v en la terminal que lo ejecuta.

El archivo "comandos.md" contiene la lista de comandos que utilicé para cada inciso del trabajo práctico, en orden. Los comandos fueron ejecutados en la terminal de un sistema operativo Ubuntu 20.04. El directorio de trabajo inicial corresponde a "tp3-data", la copia local del directorio clonado para trabajar en esta práctica.


### Autor: Valenzuela, Juan Manuel.
